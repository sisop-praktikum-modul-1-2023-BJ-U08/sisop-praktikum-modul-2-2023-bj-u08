#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <time.h>
#include <dirent.h>
#include <string.h>
#include <limits.h>


void split_animals(const char *dir_path)
{
    // Create the subdirectories if they don't exist
    const char *subdirs[] = {"darat", "amphibi", "air"};
    for (int i = 0; i < 3; i++) 
    {
        if (mkdir(subdirs[i], 0777) == -1) 
        {
            // The directory already exists or there was an error creating it
            continue;
        }
    }

    // Iterate over the files in the directory
    DIR *dir = opendir(dir_path);
    if (dir == NULL) 
    {
        perror("opendir");
        return;
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) 
    {
        // Ignore . and .. entries
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) 
        {
            continue;
        }

        // Construct the full path to the file
        char path[PATH_MAX];
        snprintf(path, PATH_MAX, "%s/%s", dir_path, entry->d_name);

        // Determine the category of the animal
        const char *category;
        if (strstr(entry->d_name, "darat") != NULL) 
        {
            category = "darat";
        } 
        else if (strstr(entry->d_name, "amphibi") != NULL) 
        {
            category = "amphibi";
        } 
        else if (strstr(entry->d_name, "air") != NULL) 
        {
            category = "air";
        } 
        else 
        {
            // Skip files that don't match any category
            continue;
        }

        // Move the file to the appropriate subdirectory
        char dest_path[PATH_MAX];
        snprintf(dest_path, PATH_MAX, "%s/%s", category, entry->d_name);
        if (rename(path, dest_path) == -1) 
        {
            perror("rename");
            continue;
        }
    }

    closedir(dir);
}

void randomise_animals(char *basePath)
{
    srand(time(NULL));

    DIR *dir = opendir(basePath);
    if (dir == NULL) 
    {
        printf("Failed to open directory\n");
        return;
    }

    int count = 0;
    struct dirent *dp;
    while ((dp = readdir(dir)) != NULL) 
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) 
        {
            count++;
        }
    }

    if (count == 0) 
    {
        printf("No files found in directory\n");
        return;
    }

    int randomIndex = (rand() % count) + 1;
    count = 1;
    rewinddir(dir);

    while ((dp = readdir(dir)) != NULL) 
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) 
        {
            if (count == randomIndex) 
            {
                printf("\nShift: %s\n\n", dp->d_name);
                break;
            }
            count++;
        }
    }

    closedir(dir);
}

int main() 
{
    pid_t pid1, pid2, pid3, pid4, pid5, pid6, pid7;
    int status;

    // Download binatang.zip
    if ((pid1 = fork()) < 0) 
    {
        perror("Error: Fork failed");
        exit(1);
    } 

    else if (pid1 == 0) 
    {
        printf("Child process with PID %d\n", getpid());
        execlp("wget", "wget", "--no-check-certificate", "-O", "binatang.zip",
            "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", NULL);
    }

    // Wait for download to finish
    wait(&status);

    // Unzip binatang.zip to animaldir/
    if ((pid2 = fork()) < 0) 
    {
        perror("Error: Fork failed");
        exit(1);
    } 

    else if (pid2 == 0) 
    {
        printf("Child process with PID %d\n", getpid());
        execlp("unzip", "unzip", "binatang.zip", "-d", "animaldir", NULL);
    }

    // Wait for unzip to finish
    wait(&status);

    // Determine shift, split animals, and delete binatang.zip
    if ((pid3 = fork()) < 0) 
    {
        perror("Error: Fork failed");
        exit(1);
    } 

    else if (pid3 == 0) 
    {
        //printf("Child process with PID %d\n", getpid()); //troubleshooting
        randomise_animals("animaldir");
        split_animals("animaldir");
        execlp("rm", "rm", "binatang.zip", NULL);
    }

    // Wait for operations to finish
    wait(&status);

    if ((pid4 = fork()) < 0) 
    {
        perror("Error: Fork failed");
        exit(1);
    } 

    else if (pid4 == 0) 
    {
        //printf("Child process with PID %d\n", getpid()); //troubleshooting
        execlp("zip", "zip", "-r", "air.zip", "air", NULL);
    }

    // Wait for zip to finish
    wait(&status);

    if ((pid5 = fork()) < 0) 
    {
        perror("Error: Fork failed");
        exit(1);
    } 

    else if (pid5 == 0) 
    {
        //printf("Child process with PID %d\n", getpid()); //troubleshooting
        execlp("zip", "zip", "-r", "amphibi.zip", "amphibi", NULL);
    }

    // Wait for zip to finish
    wait(&status);

    if ((pid6 = fork()) < 0) 
    {
        perror("Error: Fork failed");
        exit(1);
    } 

    else if (pid6 == 0) 
    {
        //printf("Child process with PID %d\n", getpid()); //troubleshooting
        execlp("zip", "zip", "-r", "darat.zip", "darat", NULL);
    }

    // Wait for zip to finish
    wait(&status);

    // Zip air/, amphibi/, and darat/ to binatang-archive.zip
    if ((pid7 = fork()) < 0) 
    {
        perror("Error: Fork failed");
        exit(1);
    } 

    else if (pid7 == 0) 
    {
        //printf("Child process with PID %d\n", getpid()); //troubleshooting
        execlp("zip", "zip", "-r", "binatang-archive.zip", "air.zip", "amphibi.zip", "darat.zip", NULL);
    }

    // Wait for zip to finish
    wait(&status);

    // Delete air/, amphibi/, darat/,and their zip versions, and also removes animaldir/ 
    execlp("rm", "rm", "-rf", "air", "amphibi", "darat", "animaldir","air.zip", "amphibi.zip", "darat.zip", NULL);

    return 0;
}
