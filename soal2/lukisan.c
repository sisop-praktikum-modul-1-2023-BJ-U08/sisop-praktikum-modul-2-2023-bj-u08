#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include <sys/wait.h>
#include <wait.h>
#include <string.h>
#include <dirent.h>

char* getTimeStamp() {
	time_t current_time;
	char *timestamp = malloc(sizeof(char) * 20);
	struct tm *time_info;

	time(&current_time);
	time_info = localtime(&current_time);
	strftime(timestamp, 20, "%Y-%m-%d_%H:%M:%S", time_info);

	return timestamp;
}

void compressAndRemove(char *x) {
	char buf[1024];
	char path[1124];
	getcwd(buf, sizeof(buf));
	sprintf(path, "%s/%s",buf, x);

	char zipFile[1124];
	sprintf(zipFile, "%s.zip", x);

	pid_t child_id;
	int status;

	child_id = fork();

	if (child_id < 0) {
		exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	}
	if (child_id == 0) {
		// this is child
		printf("Compressing folder...\n");
		char *argv[] = {"zip", "-r", zipFile, x, NULL};
		int ret = execv("/usr/bin/zip", argv);
		if (ret == -1) {
			perror("Error executing zip command");
		}
	} else {
		// this is parent
		printf("Removing folder...\n");
		while ((wait(&status)) > 0);
		char *argv[] = {"rm", "-r", path, NULL};
		int ret = execv("/bin/rm", argv);
		if (ret == -1) {
			perror("Error executing rm command");
		}
	}
}

void downloadPhoto(char *x) {
	char buf[1024];
	char path[1124];
	getcwd(buf, sizeof(buf));
	sprintf(path, "%s/%s",buf, x);

	if (chdir(path) == 0) {
		// download photo
		time_t now = (time(NULL)%1000)+50;
		for(int i=1; i<=15; i++) {
			char url[100];
			sprintf(url, "https://picsum.photos/%ld", now);
			char filename[50];
			sprintf(filename, "%s.jpg", getTimeStamp());

			pid_t pid = fork();
			if (pid == 0) {
				char *argv[] = {"wget", url, "-O", filename, "-q", NULL};
				int ret = execv("/bin/wget", argv);
				if (ret == -1) {
                    			perror("Error executing wget command");
                		}
			}

			printf("Download foto ke %d\n", i);
			sleep(5); // every 5 seconds
		}

		// init path
		chdir("..");

		compressAndRemove(x);
	}
}

void bash_command(char *command, char *argv[]) {
	pid_t child = fork();
	int status;

	if (child < 0) {
		printf("Error: fork failed!\n");
		exit(EXIT_FAILURE);
	} else if (child > 1) {
		wait(&status);
	} else if (child == 0) {
		execv(command, argv);
		exit(EXIT_SUCCESS);
	}
}

void killer(int x, char *mode) {
	chdir("..");
	FILE *baru;
	baru = fopen("baru.c", "w");
	fputs("#include <stdio.h>\n#include <signal.h>\n#include <sys/types.h>\nint main(){\n", baru);
	if (strcmp(mode, "-a") == 0) {
		fprintf(baru, "kill(-%d, SIGKILL);\nremove(\"killer\");\n}\n", x);
	} else {
		fprintf(baru, "kill(%d, SIGTERM);\nremove(\"killer\");\n}\n", x);
	}
//	fputs("}\n", baru);
	fclose(baru);

	char *argv[] = {"gcc", "baru.c", "-o", "kill", NULL};
	char *removeFile[] = {"rm", "baru.c", NULL};
	bash_command("/usr/bin/gcc", argv);
	bash_command("/usr/bin/rm", removeFile);
	chdir("folder_khusus");
}

int main(int argc, char **argv) {
	if (argc < 2) {
		exit(EXIT_SUCCESS);
	} else {
		DIR *dir = opendir("folder_khusus");
		if (!dir) {
			mkdir("folder_khusus", 0777);
		}
		closedir(dir);

		chdir("folder_khusus");

		pid_t pid, sid;        // Variabel untuk menyimpan PID

		pid = fork();     // Menyimpan PID dari Child Process

		/* Keluar saat fork gagal
		* (nilai variabel pid < 0) */
		if (pid < 0) { exit(EXIT_FAILURE); }

		/* Keluar saat fork berhasil
		* (nilai variabel pid adalah PID dari child process) */
		if (pid > 0) { exit(EXIT_SUCCESS); }

		umask(0);

		sid = setsid();
		if (sid < 0) { exit(EXIT_FAILURE); }

		close(STDIN_FILENO);
		close(STDOUT_FILENO);
		close(STDERR_FILENO);

		pid_t child_pid;

		if (strcmp(argv[1], "-a") == 0) {
			killer(sid, "-a");
		}
		else {
			killer(sid, "-b");
		}

		while(1) {
			child_pid = fork();

			if (child_pid == 0) {
				char *y = getTimeStamp();
				mkdir(y, 0777); // make new folder with timestamp name format
				printf("A new directory %s has been made!\n", y);
				downloadPhoto(y);
				free(y);
			}
			else if (child_pid > 0) {
				sleep(30);
			}
			else {
				// fork() failed
				printf("Failed to create child process.\n");
				exit(1);
			}
		}
	}

    return 0;
}
