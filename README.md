# sisop-praktikum-modul-2-2023-BJ-U08


## Group Members


| Name | NRP |
| --- | --- |
| Mochammad Naufal Ihza Syahzada  | 5025211260 |
| Ariel Pratama Menlolo | 5025211194 |
| Teuku Aulia Azhar | 5025201142 |

## Task Check

- [x] soal1
- [x] soal2
- [x] soal3 - [link revisi](https://drive.google.com/drive/folders/1zSkWgutzrG17_tSvxf0F-T17CSep0o3n?usp=sharing)
- [x] soal4

## Number 1 Solution

### (A)

Download the file from the pre-given link
```c
    if ((pid1 = fork()) < 0) 
    {
        perror("Error: Fork failed");
        exit(1);
    } 

    else if (pid1 == 0) 
    {
        printf("Child process with PID %d\n", getpid());
        execlp("wget", "wget", "--no-check-certificate", "-O", "binatang.zip",
            "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", NULL);
    }
```
Unzip the file
```c
if ((pid2 = fork()) < 0) 
    {
        perror("Error: Fork failed");
        exit(1);
    } 

    else if (pid2 == 0) 
    {
        printf("Child process with PID %d\n", getpid());
        execlp("unzip", "unzip", "binatang.zip", "-d", "animaldir", NULL);
    }
```

### (B)

Picking a random animal (file) from the list (with function which will then be called in the main function)
```c
void randomise_animals(char *basePath)
{
    srand(time(NULL));

    DIR *dir = opendir(basePath);
    if (dir == NULL) 
    {
        printf("Failed to open directory\n");
        return;
    }

    int count = 0;
    struct dirent *dp;
    while ((dp = readdir(dir)) != NULL) 
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) 
        {
            count++;
        }
    }

    if (count == 0) 
    {
        printf("No files found in directory\n");
        return;
    }

    int randomIndex = (rand() % count) + 1;
    count = 1;
    rewinddir(dir);

    while ((dp = readdir(dir)) != NULL) 
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) 
        {
            if (count == randomIndex) 
            {
                printf("\nShift: %s\n\n", dp->d_name);
                break;
            }
            count++;
        }
    }

    closedir(dir);
}

```

### (C)

Makes 3 Directories (darat, amphibi, air) and filter the file names and inserts each file to each respected directory
(using a function which will then be called in the main() function)
```c
void split_animals(const char *dir_path)
{
    // Create the subdirectories if they don't exist
    const char *subdirs[] = {"darat", "amphibi", "air"};
    for (int i = 0; i < 3; i++) 
    {
        if (mkdir(subdirs[i], 0777) == -1) 
        {
            // The directory already exists or there was an error creating it
            continue;
        }
    }

    // Iterate over the files in the directory
    DIR *dir = opendir(dir_path);
    if (dir == NULL) 
    {
        perror("opendir");
        return;
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) 
    {
        // Ignore . and .. entries
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) 
        {
            continue;
        }

        // Construct the full path to the file
        char path[PATH_MAX];
        snprintf(path, PATH_MAX, "%s/%s", dir_path, entry->d_name);

        // Determine the category of the animal
        const char *category;
        if (strstr(entry->d_name, "darat") != NULL) 
        {
            category = "darat";
        } 
        else if (strstr(entry->d_name, "amphibi") != NULL) 
        {
            category = "amphibi";
        } 
        else if (strstr(entry->d_name, "air") != NULL) 
        {
            category = "air";
        } 
        else 
        {
            // Skip files that don't match any category
            continue;
        }

        // Move the file to the appropriate subdirectory
        char dest_path[PATH_MAX];
        snprintf(dest_path, PATH_MAX, "%s/%s", category, entry->d_name);
        if (rename(path, dest_path) == -1) 
        {
            perror("rename");
            continue;
        }
    }

    closedir(dir);
}

```

### (D)

Zips every file then puts all the zipped file to a new file named "binatang-archive.zip" which will then also be zipped again, finally it deletes all the other files so that only "binatang-archive.zip" remains.
```c
if ((pid4 = fork()) < 0) 
    {
        perror("Error: Fork failed");
        exit(1);
    } 

    else if (pid4 == 0) 
    {
        //printf("Child process with PID %d\n", getpid()); //troubleshooting
        execlp("zip", "zip", "-r", "air.zip", "air", NULL);
    }

    // Wait for zip to finish
    wait(&status);

    if ((pid5 = fork()) < 0) 
    {
        perror("Error: Fork failed");
        exit(1);
    } 

    else if (pid5 == 0) 
    {
        //printf("Child process with PID %d\n", getpid()); //troubleshooting
        execlp("zip", "zip", "-r", "amphibi.zip", "amphibi", NULL);
    }

    // Wait for zip to finish
    wait(&status);

    if ((pid6 = fork()) < 0) 
    {
        perror("Error: Fork failed");
        exit(1);
    } 

    else if (pid6 == 0) 
    {
        //printf("Child process with PID %d\n", getpid()); //troubleshooting
        execlp("zip", "zip", "-r", "darat.zip", "darat", NULL);
    }

    // Wait for zip to finish
    wait(&status);

    // Zip air/, amphibi/, and darat/ to binatang-archive.zip
    if ((pid7 = fork()) < 0) 
    {
        perror("Error: Fork failed");
        exit(1);
    } 

    else if (pid7 == 0) 
    {
        //printf("Child process with PID %d\n", getpid()); //troubleshooting
        execlp("zip", "zip", "-r", "binatang-archive.zip", "air.zip", "amphibi.zip", "darat.zip", NULL);
    }

    // Wait for zip to finish
    wait(&status);

    // Delete air/, amphibi/, darat/,and their zip versions, and also removes animaldir/ 
    execlp("rm", "rm", "-rf", "air", "amphibi", "darat", "animaldir","air.zip", "amphibi.zip", "darat.zip", NULL);

    return 0;

```

## Number 2 Solution

### (A)

```c
char* getTimeStamp() {
	time_t current_time;
	char *timestamp = malloc(sizeof(char) * 20);
	struct tm *time_info;

	time(&current_time);
	time_info = localtime(&current_time);
	strftime(timestamp, 20, "%Y-%m-%d_%H:%M:%S", time_info);

	return timestamp;
}
```
untuk mendapatkan format nama folder
```c
DIR *dir = opendir("folder_khusus");
if (!dir) {
	mkdir("folder_khusus", 0777);
}
closedir(dir);
chdir("folder_khusus");
```
membuat folder khusus
```c
while(1) {
    child_pid = fork();

    if (child_pid == 0) {
        char *y = getTimeStamp();
        mkdir(y, 0777); // make new folder with timestamp name format
        printf("A new directory %s has been made!\n", y);
        downloadPhoto(y);
        free(y);
    }
    else if (child_pid > 0) {
        sleep(30);
    }
    else {
        // fork() failed
        printf("Failed to create child process.\n");
        exit(1);
    }
}
```
program berjalan setiap 30 detik ketika masuk proses parent

### (B)

```c
void downloadPhoto(char *x) {
	char buf[1024];
	char path[1124];
	getcwd(buf, sizeof(buf));
	sprintf(path, "%s/%s",buf, x);

	if (chdir(path) == 0) {
		// download photo
		time_t now = (time(NULL)%1000)+50;
		for(int i=1; i<=15; i++) {
			char url[100];
			sprintf(url, "https://picsum.photos/%ld", now);
			char filename[50];
			sprintf(filename, "%s.jpg", getTimeStamp());

			pid_t pid = fork();
			if (pid == 0) {
				char *argv[] = {"wget", url, "-O", filename, "-q", NULL};
				int ret = execv("/bin/wget", argv);
				if (ret == -1) {
                	perror("Error executing wget command");
                }
			}

			printf("Download foto ke %d\n", i);
			sleep(5); // every 5 seconds
		}

		// init path
		chdir("..");

		compressAndRemove(x);
	}
}
```
Function ini akan terpanggil untuk menjawab pertanyaan (b). Dan agar berjalan secara overlapping maka diperlukan adanya fork. Foto akan ter download setiap 5 detik sesuai dengan command sleep(5).

### (C)

```c
void compressAndRemove(char *x) {
	char buf[1024];
	char path[1124];
	getcwd(buf, sizeof(buf));
	sprintf(path, "%s/%s",buf, x);

	char zipFile[1124];
	sprintf(zipFile, "%s.zip", x);

	pid_t child_id;
	int status;

	child_id = fork();

	if (child_id < 0) {
		exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	}
	if (child_id == 0) {
		// this is child
		printf("Compressing folder...\n");
		char *argv[] = {"zip", "-r", zipFile, x, NULL};
		int ret = execv("/usr/bin/zip", argv);
		if (ret == -1) {
			perror("Error executing zip command");
		}
	} else {
		// this is parent
		printf("Removing folder...\n");
		while ((wait(&status)) > 0);
		char *argv[] = {"rm", "-r", path, NULL};
		int ret = execv("/bin/rm", argv);
		if (ret == -1) {
			perror("Error executing rm command");
		}
	}
}
```
Function ini akan menjalankan proses compress di child lalu dilanjutkan proses penghapusan foler di pareng. Parent akan menunggu proses child selesai.

### (D) && (E)
```c
void bash_command(char *command, char *argv[]) {
	pid_t child = fork();
	int status;

	if (child < 0) {
		printf("Error: fork failed!\n");
		exit(EXIT_FAILURE);
	} else if (child > 1) {
		wait(&status);
	} else if (child == 0) {
		execv(command, argv);
		exit(EXIT_SUCCESS);
	}
}

void killer(int x, char *mode) {
	chdir("..");
	FILE *baru;
	baru = fopen("baru.c", "w");
	fputs("#include <stdio.h>\n#include <signal.h>\n#include <sys/types.h>\nint main(){\n", baru);
	if (strcmp(mode, "-a") == 0) {
		fprintf(baru, "kill(-%d, SIGKILL);\nremove(\"killer\");\n}\n", x);
	} else {
		fprintf(baru, "kill(%d, SIGTERM);\nremove(\"killer\");\n}\n", x);
	}
//	fputs("}\n", baru);
	fclose(baru);

	char *argv[] = {"gcc", "baru.c", "-o", "kill", NULL};
	char *removeFile[] = {"rm", "baru.c", NULL};
	bash_command("/usr/bin/gcc", argv);
	bash_command("/usr/bin/rm", removeFile);
	chdir("folder_khusus");
}
```
Terdapat dua function. Function bash_command untuk menjalankan perintah di killer sehingga bisa berjalan bertahap. Function killer untuk men-generate file yang bisa menghentikan program yang berjalan. Terdapat dua jenis cara untuk menghentikan program, yaitu dengan menghentikannya secara langsung "-a" atau menunggu file terakhir yang bekerja berhenti lalu berhenti "-b".


## Number 3 Solution

### (A)
```c
char* download_argv[] = { "wget","-q", "--no-check-certificate", "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", "-O", "players.zip", NULL };
char* unzip_argv[] = { "unzip", "-n", "-q", "players.zip",  NULL };
char* remove_zip[] = { "rm", "players.zip", NULL };
bash_command(download_argv, "/bin/wget");
bash_command(unzip_argv, "/bin/unzip");
bash_command(remove_zip, "/bin/rm");
```
Kode diatas akan download foto dari link drive. Karena dalam bentuk zip, maka program diatas akan meng-ekstrak file zip tersebut lalu menghapus file zip tersebut.

### (B)
```c
chdir("players");
char* bukanMU[] = { "find", "-type", "f", "!", "-name", "*_ManUtd_*", "-delete", NULL };
bash_command(bukanMU, "/bin/find");
chdir("..");
```
Digunakan untuk menghapus semua pemain yang bukan "ManUtd"


### (C)
```c
char* mkdir_argv[] = { "install", "-d", "Bek", "Penyerang", "Gelandang", "Kiper", NULL };
bash_command(mkdir_argv, "/bin/install");

char* pindahBek[] = { "find", "-type", "f", "-name", "*_Bek_*", "-exec", "mv", "-t", "Bek", "{}", "+", NULL };
char* pindahGelandang[] = { "find", "-type", "f", "-name", "*_Gelandang_*", "-exec", "mv", "-t", "Gelandang", "{}", "+", NULL };
char* pindahPenyerang[] = { "find", "-type", "f", "-name", "*_Penyerang_*", "-exec", "mv", "-t", "Penyerang", "{}", "+", NULL };
char* pindahKiper[] = { "find", "-type", "f", "-name", "*_Kiper_*", "-exec", "mv", "-t", "Kiper", "{}", "+", NULL };
bash_command(pindahBek, "/bin/find");
bash_command(pindahGelandang, "/bin/find");
bash_command(pindahPenyerang, "/bin/find");
bash_command(pindahKiper, "/bin/find");

char* rmv_dir[] = { "rm", "-r", "players", NULL };
bash_command(rmv_dir, "/bin/rm");
```
Untuk mengategorikan pemain sesuai posisi menggunakan kode diatas. Urutan prosesnya adalah mencari lalu memindahkannya langsung ke folder baru sesuai dengan posisinya. Kedua proses tersebut terjadi di simpang dalam satu char, hingga ketika bash_command dijalankan kedua proses berjalan berbarengan. Ketika semua sudah dipindah maka directory atau folder tadi akan dihapus.

### (D)
```c
void buatTim(int b, int g, int p) {
	char bek_argv[79], gelandang_argv[85], penyerang_argv[85], kiper_argv[85];
  	char* username = getenv("USER");

  	snprintf(bek_argv, sizeof(bek_argv), "ls Bek | sort -nr -t _ -k 4 | head -n %d >> /home/%s/Formasi_%d-%d-%d.txt", b, username, b, g, p);
	snprintf(gelandang_argv, sizeof(gelandang_argv), "ls Gelandang | sort -nr -t _ -k 4 | head -n %d >> /home/%s/Formasi_%d-%d-%d.txt", g, username, b, g, p);
  	snprintf(penyerang_argv, sizeof(penyerang_argv), "ls Penyerang | sort -nr -t _ -k 4 | head -n %d >> /home/%s/Formasi_%d-%d-%d.txt", p, username, b, g, p);
  	snprintf(kiper_argv, sizeof(kiper_argv), "ls Kiper | sort -nr -t _ -k 4 | head -n 1 >> /home/%s/Formasi_%d-%d-%d.txt", username, b, g, p);

	char* bek[] = { "bash", "-c", bek_argv, NULL };
  	char* gelandang[] = { "bash", "-c", gelandang_argv, NULL };
  	char* penyerang[] = { "bash", "-c", penyerang_argv, NULL };
  	char* kiper[] = { "bash", "-c", kiper_argv, NULL };

  	bash_command(bek, "/bin/bash");
  	bash_command(gelandang, "/bin/bash");
  	bash_command(penyerang, "/bin/bash");
  	bash_command(kiper, "/bin/bash");
}
```

Dengan memanggil fungsi ini, program akan memanggil pemain sebanyak parameter yang dimasukkan yang nantinya akan memilih pemain terbaik (setelah proses pengurutan). Semua data tersebut akan tercatat pada file .txt di posisi home/[user]
