#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include <sys/wait.h>
#include <wait.h>
#include <string.h>
#include <dirent.h>
#include <errno.h>

/*
void bash_command(char *command, char *argv[]) {
	pid_t child = fork();
	int status;

	if (child < 0) {
		printf("Error: fork failed!\n");
		exit(EXIT_FAILURE);
	} else if (child > 1) {
		wait(&status);
	} else if (child == 0) {
		execv(command, argv);
		exit(EXIT_SUCCESS);
	}
}
*/

void bash_command(char* argv[], char* command_execute) {
  pid_t id_child = fork();
  int status;

  if (id_child < 0) {
    printf("Error: Fork Failed\n");
    exit(1);
  }
  else if (id_child == 0) {
    execv(command_execute, argv);
    exit(EXIT_SUCCESS);
  }
  else wait(&status);
}


/*
void bash_command(char *command, char *argv[]) {
    pid_t pid = fork();
    if (pid == 0) {
        execvp(command, argv);
        perror("execvp failed");
        exit(1);
    } else if (pid < 0) {
        perror("fork failed");
    } else {
        int status;
        waitpid(pid, &status, 0);
    }
}
*/

void buatTim(int b, int g, int p) {
	char bek_argv[79], gelandang_argv[85], penyerang_argv[85], kiper_argv[85];
  	char* username = getenv("USER");

  	snprintf(bek_argv, sizeof(bek_argv), "ls Bek | sort -nr -t _ -k 4 | head -n %d >> /home/%s/Formasi_%d-%d-%d.txt", b, username, b, g, p);
	snprintf(gelandang_argv, sizeof(gelandang_argv), "ls Gelandang | sort -nr -t _ -k 4 | head -n %d >> /home/%s/Formasi_%d-%d-%d.txt", g, username, b, g, p);
  	snprintf(penyerang_argv, sizeof(penyerang_argv), "ls Penyerang | sort -nr -t _ -k 4 | head -n %d >> /home/%s/Formasi_%d-%d-%d.txt", p, username, b, g, p);
  	snprintf(kiper_argv, sizeof(kiper_argv), "ls Kiper | sort -nr -t _ -k 4 | head -n 1 >> /home/%s/Formasi_%d-%d-%d.txt", username, b, g, p);

	char* bek[] = { "bash", "-c", bek_argv, NULL };
  	char* gelandang[] = { "bash", "-c", gelandang_argv, NULL };
  	char* penyerang[] = { "bash", "-c", penyerang_argv, NULL };
  	char* kiper[] = { "bash", "-c", kiper_argv, NULL };

  	bash_command(bek, "/bin/bash");
  	bash_command(gelandang, "/bin/bash");
  	bash_command(penyerang, "/bin/bash");
  	bash_command(kiper, "/bin/bash");
}

int main() {
  	char* download_argv[] = { "wget","-q", "--no-check-certificate", "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", "-O", "players.zip", NULL };
  	char* unzip_argv[] = { "unzip", "-n", "-q", "players.zip",  NULL };
  	char* remove_zip[] = { "rm", "players.zip", NULL };
  	bash_command(download_argv, "/bin/wget");
	bash_command(unzip_argv, "/bin/unzip");
	bash_command(remove_zip, "/bin/rm");

  	chdir("players");
  	char* bukanMU[] = { "find", "-type", "f", "!", "-name", "*_ManUtd_*", "-delete", NULL };
  	bash_command(bukanMU, "/bin/find");
  	chdir("..");

  	char* mkdir_argv[] = { "install", "-d", "Bek", "Penyerang", "Gelandang", "Kiper", NULL };
  	bash_command(mkdir_argv, "/bin/install");

  	char* pindahBek[] = { "find", "-type", "f", "-name", "*_Bek_*", "-exec", "mv", "-t", "Bek", "{}", "+", NULL };
  	char* pindahGelandang[] = { "find", "-type", "f", "-name", "*_Gelandang_*", "-exec", "mv", "-t", "Gelandang", "{}", "+", NULL };
  	char* pindahPenyerang[] = { "find", "-type", "f", "-name", "*_Penyerang_*", "-exec", "mv", "-t", "Penyerang", "{}", "+", NULL };
  	char* pindahKiper[] = { "find", "-type", "f", "-name", "*_Kiper_*", "-exec", "mv", "-t", "Kiper", "{}", "+", NULL };
	bash_command(pindahBek, "/bin/find");
	bash_command(pindahGelandang, "/bin/find");
	bash_command(pindahPenyerang, "/bin/find");
	bash_command(pindahKiper, "/bin/find");

  	char* rmv_dir[] = { "rm", "-r", "players", NULL };
  	bash_command(rmv_dir, "/bin/rm");

  	buatTim(4, 3, 3);
}
